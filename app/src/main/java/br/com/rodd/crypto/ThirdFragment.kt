package br.com.rodd.crypto

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.lifecycleScope
import br.com.rodd.crypto.model.User
import br.com.rodd.crypto.network.RetrofitInitializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ThirdFragment : Fragment() {

       override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_third, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        view.findViewById<Button>(R.id.buttonPost).setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO){
                val user = User("nascimentorodrigo120@gmail.com", "12345678", "adm")
                val response = RetrofitInitializer().APIEndpoints().sendLogin(user)
                if (response.isSuccessful){
                    timber(response.body().toString())
                }else{
                    timber(response.errorBody()?.string()!!)
                }
            }
        }

        view.findViewById<Button>(R.id.buttonGet).setOnClickListener {

        }
    }


    private fun timber(msg: String){
        Log.i("RESPONSE", msg)
    }

}
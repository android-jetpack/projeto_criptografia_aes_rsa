package br.com.rodd.crypto.network

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

import android.text.TextUtils
import android.util.Log

import br.com.rodd.crypto.crypto.AESCrypto
import okhttp3.MediaType
import java.io.IOException
import java.lang.Exception
import java.lang.IllegalArgumentException


class DecryptResponseInterceptor(private val mDecryptionStrategy: AESCrypto?) : Interceptor {

    private val TAG: String = "ENCRY_DECRY"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        timber("===============DECRYPTING RESPONSE===============")
        val response = chain.proceed(chain.request())
        if (response.isSuccessful) {
            val newResponse = response.newBuilder()
            var contentType = response.header("Content-Type")
            if (TextUtils.isEmpty(contentType)) contentType = "application/json"
//            val responseString = response.body()!!.string()
            val responseString = EncryptRequestInterceptor.encryptRequest
            var decryptedString: String? = null

            if (mDecryptionStrategy != null) {
                try {
                    decryptedString = mDecryptionStrategy.decrypt(responseString)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                timber("Response string ===> %s $responseString")
                timber("Decrypted BODY  ===> %s $decryptedString")
            } else {
                throw IllegalArgumentException("No decryption strategy!")
            }
            newResponse.body(ResponseBody.create(MediaType.parse(contentType), decryptedString))
            return newResponse.build()
        }
        return response
    }

    private fun timber(msg: String){
        Log.i(TAG, msg)
    }
}
package br.com.rodd.crypto.network

import br.com.rodd.crypto.model.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

// serviço online para mockar API: https://tempapi.proj.me/

interface ApiEndpoints {

//    @POST("fWRJlx7VM")
    @POST("uEyWRkiN9")
    suspend fun sendLogin(@Body userRequest: User): Response<User>
}
package br.com.rodd.crypto.network

import android.util.Log
import br.com.rodd.crypto.crypto.AESCrypto
import okhttp3.*
import java.io.IOException
import java.lang.Exception
import java.lang.IllegalArgumentException


class EncryptRequestInterceptor(private val mEncryptionStrategy: AESCrypto?) : Interceptor {


    private val TAG: String = "ENCRY_DECRY"

    //Par teste (o certo seria o sevidor enviar)
    companion object{
        var encryptRequest = ""
    }


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        timber("===============ENCRYPTING REQUEST===============")
        var request: Request = chain.request()
        val rawBody: RequestBody? = request.body()
        var encryptedBody: String? = ""
        val mediaType: MediaType? = MediaType.parse("text/plain; charset=utf-8")
        if (mEncryptionStrategy != null) {
            try {
                val rawBodyString: String = CryptoUtil.requestBodyToString(rawBody)
                    ?: throw IllegalArgumentException("RequestBody emptry!")

                encryptedBody = mEncryptionStrategy.encrypt(rawBodyString)
                timber("Raw body ====> %s $rawBodyString")
                timber("Encrypted BODY ====> %s $encryptedBody")

                //Para teste
                encryptRequest = encryptedBody

            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            throw IllegalArgumentException("No encryption strategy!")
        }

        val body: RequestBody = RequestBody.create(mediaType, encryptedBody)

        request = request.newBuilder()
            .header("Content-Type", body.contentType().toString())
            .method(request.method(), body).build()
        return chain.proceed(request)
    }

    private fun timber(msg: String){
        Log.i(TAG, msg)
    }
}
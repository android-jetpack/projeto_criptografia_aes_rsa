package br.com.rodd.crypto.network

import br.com.rodd.crypto.crypto.AESCrypto
import okhttp3.OkHttpClient

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInitializer(baseUrl : String = "https://tempapi.proj.me/api/") {

    private var aesCrypto: AESCrypto? = null

    private var okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(EncryptRequestInterceptor(getInstanceAES()))
            .addInterceptor(DecryptResponseInterceptor(getInstanceAES()))
            .build()

    private  val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()



    private fun getInstanceAES(): AESCrypto{
        if (aesCrypto == null){
            aesCrypto = AESCrypto()
        }
        return aesCrypto!!
    }


    fun APIEndpoints(): ApiEndpoints{
        return retrofit.create(ApiEndpoints::class.java)
    }
}
package br.com.rodd.crypto.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("email")
    var email: String = "",
    @SerializedName("password")
    var password: String = "",
    @SerializedName("rule")
    var rule: String = ""){


    override fun toString(): String {
        return "email: $email\nsenha: $password\nrule: $rule"
    }
}

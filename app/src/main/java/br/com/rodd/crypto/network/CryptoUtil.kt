package br.com.rodd.crypto.network

import okhttp3.RequestBody
import okio.Buffer
import java.io.IOException


object CryptoUtil {

    fun requestBodyToString(request: RequestBody?): String? {
        return try {
            if(request == null)
                return null

            val buffer = Buffer()
            request.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            throw e
        }
    }
}